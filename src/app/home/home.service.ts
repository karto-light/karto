import { Injectable } from "@angular/core";
import { Subject } from "rxjs";



@Injectable({
    providedIn: "root"
})
export class HomeService{

    public events = new Subject<HomeEvent>();



    addFixture(){
        this.events.next({
            type: "addFixture"
        })
    }

}

export interface HomeEvent{
    type: "addFixture" | "closeAddFixture"
}