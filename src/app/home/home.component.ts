import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import * as fs from "fs";
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  // @ViewChild("panelHost") panelHost: ViewContainerRef;

  isAddingFixture: boolean = false;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    // console.log(fs.readdirSync(__dirname), __dirname)
    this.homeService.events.subscribe(e => {
      if(e.type == "addFixture"){
        this.isAddingFixture = true;
      }
      if(e.type == "closeAddFixture"){
        this.isAddingFixture = false;
      }
    })
  }


}
