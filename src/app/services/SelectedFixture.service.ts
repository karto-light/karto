import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Fixture } from '../../../lib/layout';

@Injectable({
  providedIn: 'root'
})
export class SelectedFixtureService {

  fixtureSelection: Subject<Map<number, boolean>> = new Subject()


  constructor() { }



}


