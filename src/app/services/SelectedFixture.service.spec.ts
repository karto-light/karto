/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SelectedFixtureService } from './SelectedFixture.service';

describe('Service: SelectedFixture', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectedFixtureService]
    });
  });

  it('should ...', inject([SelectedFixtureService], (service: SelectedFixtureService) => {
    expect(service).toBeTruthy();
  }));
});
