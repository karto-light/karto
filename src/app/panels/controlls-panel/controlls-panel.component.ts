import { Component, OnInit, Input } from '@angular/core';
import { SelectedFixtureService } from '../../services/SelectedFixture.service';
import { layout } from '../../../../lib';
import { Fixture } from '../../../../lib/layout';
import { Controll } from '../../../../lib/patches';

@Component({
  selector: 'app-controlls-panel',
  templateUrl: './controlls-panel.component.html',
  styleUrls: ['./controlls-panel.component.less']
})
export class ControllsPanelComponent implements OnInit {

  private _selectedFixtures = new Map<number, boolean>();

  constructor(private fixture: SelectedFixtureService) { }

  ngOnInit() {
    this.fixture.fixtureSelection.subscribe(d => {
      this._selectedFixtures = d;
    })
  }



  getFixture(id: number) {
    return layout.get(id);
  }

  get selectedFixtures(): Array<Fixture> {
    var a = [];
    for(let e of this._selectedFixtures.entries()){
      if(e[1]) a.push(this.getFixture(e[0]))
    }
    return a;
  }

}
