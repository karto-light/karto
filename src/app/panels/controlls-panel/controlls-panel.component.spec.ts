import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControllsPanelComponent } from './controlls-panel.component';

describe('ControllsPanelComponent', () => {
  let component: ControllsPanelComponent;
  let fixture: ComponentFixture<ControllsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControllsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControllsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
