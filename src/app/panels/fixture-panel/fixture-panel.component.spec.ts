import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixturePanelComponent } from './fixture-panel.component';

describe('FixturePanelComponent', () => {
  let component: FixturePanelComponent;
  let fixture: ComponentFixture<FixturePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixturePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixturePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
