import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as fs from "fs";
import { Fixture } from '../../../../lib/layout';
import { layout } from '../../../../lib';
import { ColorPickerControll } from '../../../../lib/patches';
import { HomeService } from '../../home/home.service';
import { SelectedFixtureService } from '../../services/SelectedFixture.service';
import * as Color from "color"

@Component({
  selector: 'app-fixture-panel',
  templateUrl: './fixture-panel.component.html',
  styleUrls: ['./fixture-panel.component.less']
})
export class FixturePanelComponent implements OnInit {

  selectedFixtures = new Map<number, boolean>()
  
  fixtures: Fixture[] = [];

  constructor(private home: HomeService, private selectedFixturesS: SelectedFixtureService) {
  }

  ngOnInit() {
    this.fixtures = layout.all();
    console.log("setting color");
    (this.fixtures[2].getControll("Color Picker") as ColorPickerControll).setColor(new Color("#ffffff"))

    // this.home.addFixture()
  }


  addFixture(){
    this.home.addFixture()
    // console.log("Adding fixture")
  }

  toggleFixtureSelect(f: Fixture){
    this.selectedFixtures.set(f.id, !this.selectedFixtures.get(f.id))
    this.selectedFixturesS.fixtureSelection.next(this.selectedFixtures)
  }


}