import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { Patch, ColorPickerControll, DimmerControll } from '../../../lib/patches';
import { patches as libPatches, artnet, layout, patches } from '../../../lib';
import { HomeService } from '../home/home.service';
import { Fixture } from '../../../lib/layout';
import * as Color from "color"
import { EventEmitter } from 'events';

@Component({
  selector: 'app-add-fixture',
  templateUrl: './add-fixture.component.html',
  styleUrls: ['./add-fixture.component.less']
})
export class AddFixtureComponent implements OnInit {

  patches: Patch[] = [];
  displayedPatches: Patch[] = [];
  grid: any[][] = []

  selectedPatch: Patch = null;
  hoverX: number = 0;
  hoverY: number = 0;

  placedFixtures: {x: number, y: number, fixture: Fixture}[] = [];



  constructor(private home: HomeService) { }

  ngOnInit() {
    this.patches = libPatches.all;
    this.displayedPatches = this.patches;
    for(let y = 0; y < 16; y++){
      this.grid.push([])
      for(let x = 0; x < 32; x++){
        this.grid[y][x] = null;
      }
    }

    layout.all().forEach(f => {
      this.placedFixtures.push({
        x: f.address - Math.floor(f.address / 32) * 32 - 1,
        y: Math.floor(f.address / 32),
        fixture: f
      })
    })
  }

  gridHover(x, y){
    this.hoverX = x;
    this.hoverY = y;
  }

  select(patch: Patch){
    this.selectedPatch = this.selectedPatch == patch ? null : patch;
  }

  refPlace(ev){
    if(this.invalid(this.hoverX, this.hoverY, this.selectedPatch))
      return;
    var f = layout.add(this.selectedPatch, this.hoverY * 32 + this.hoverX + 1, null, true)
    this.placedFixtures.push({
      x: this.hoverX,
      y: this.hoverY,
      fixture: f
    })
    this.hoverX = 0;
    this.hoverY = 0;
    this.selectedPatch = null;
  }

  close(){
    this.home.events.next({
      type: "closeAddFixture"
    })
  }

  patchRefWidth(patch: Patch, x: number): string{
    if(patch.channels > 32 - x){
      return (100 / 32 * (32 - x)) + "%";
    }else{
      return (100 / 32 * patch.channels) + "%"
    }
  }

  patchRefOverhangWidth(patch: Patch, x: number): string{
    return (100 / 32 * (patch.channels - 32 + x)) + "%"
  }

  filter(query: string){
    this.displayedPatches = this.patches.filter(v => v.name.startsWith(query) || v.name.includes(query));
  }

  invalid(x, y, patch: Patch): boolean{
    var addressEnd = y * 32 + x + patch.channels;
    for(let f of this.placedFixtures){
        if(addressEnd >= f.fixture.address && addressEnd < f.fixture.address + f.fixture.patch.channels)
         return true
    }
    return false;
  }

}
