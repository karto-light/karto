import { Component, OnInit, Input } from '@angular/core';
import { Fixture } from '../../../../lib/layout';
import { Controll, DimmerControll } from '../../../../lib/patches';
import { layout } from '../../../../lib';

@Component({
  selector: 'slider-controll',
  templateUrl: './slider-controll.component.html',
  styleUrls: ['./slider-controll.component.less']
})
export class SliderControllComponent implements OnInit {

  @Input() fixture: Fixture;
  @Input() controll: DimmerControll;

  value = 0;

  constructor() { }

  ngOnInit() {
    if(this.controll.type != "dim"){
      console.error("Can not bind a " + this.controll.type + " controll to a slider controll")
    }

    console.log(this.fixture, this.controll)

  }

  onInput(event){
    this.value = parseInt(event.target.value);
    this.controll.set(this.value)
  }

}
