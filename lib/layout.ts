import { Patch, Controll } from "./patches";

export class Layout{
    
    private layout: Fixture[] = [];

    constructor(private artnet: any){

    }

    get(id: number): Fixture{
        return this.layout.find(f => f.id == id);
    }

    add(patch: Patch, address: number, id: number, setZero: boolean = false): Fixture{
        if(id == null || id < 0) id = this.layout.length;

        var f = new Fixture(this.artnet, id, patch, address)
        if(setZero){
            for(var i = 1; i <= patch.channels; i++){
                f.set(i, 0)
            }
        }
        this.layout.push(f);
        return f;
    }

    all(): Fixture[]{
        return this.layout;
    }
}

export class Fixture{
    values: {[channel: number]: number} = {}
    controlls: Controll[] = []

    constructor(private artnet: any, public id: number, public patch: Patch, public address: number = 0){
        this.initControlls(); 
    }

    private initControlls(){
        for(let c of this.patch.controlls){
            var controll = c.clone();
            controll.fixture = this;
            this.controlls.push(controll);
        }
    }

    get(channel: number): number{
        return this.values[channel]
    }

    set(channel: number, value: number){
        // console.log("Setting " + (this.address + channel - 1) + " to " + value)
        this.values[this.address + (channel - 1)] = value;
        console.log("Setting " + (this.address + (channel - 1)) + " to " + value + " [ " + this.values[this.address + (channel - 1)] + " ]")
        this.artnet.set(this.address + (channel - 1), value, (err, res) => {
            console.log(err, res)
        });
    }

    getControll<T extends Controll>(name: string): T{
        return this.controlls.find(c => c.name == name) as T;
    }

}