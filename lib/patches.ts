import * as fs from "fs";
import * as Color from "color"
import { Fixture } from "./layout";

export const PATCHES_DIR = "D:\\jonio\\Dokumente\\Programmieren\\KArtO/patches"

export class Patches{

    private patches: Patch[] = []; 

    constructor(){
        for(let f of fs.readdirSync(PATCHES_DIR, "utf8")){
            var p = JSON.parse(fs.readFileSync(PATCHES_DIR + "/" + f, "utf8"));
            
            var controlls = []
            for(let c of p.controlls){
                switch(c.type){
                    case "colorPicker": controlls.push(new ColorPickerControll(c.name, c.channels)); break;
                    case "tilt": controlls.push(new TiltControll(c.name, c.channels)); break;
                    case "dim": controlls.push(new DimmerControll(c.name, c.channels)); break;
                }
            }

            var patch: Patch = new Patch()

            patch.name = p.name;
            patch.channels = p.channels;
            patch.specifications = p.specifications;
            patch.controlls = controlls;
            patch.type = p.type;

            this.patches.push(patch)
        }
    }

    getPatch(name): Patch{
        return this.patches.find(p => p.name == name);
    }

    get all(): Patch[]{
        return this.patches;
    }

}

export class Patch{
    name: string
    channels: number
    specifications: {mode: string}[]
    controlls: Controll[]
    type: string
    

    // fixture?: Fixture
}

export interface Controll{
    type: string
    fixture?: Fixture
    name: string
    channels: { [value: string]: number }
    clone(): Controll
}

export class ColorPickerControll implements Controll{
    type: string = "colorPicker"
    color: Color = Color.rgb(0, 0, 0);

    constructor(
        public name: string = "",
        public channels: { "R": number, "G": number, "B": number, "A": number} = { R: 0, G: 0, B: 0, A: 0 },
        public fixture?: Fixture
    ){}

    setColor(color: Color){
        if(this.fixture){
            this.color = color;
            this.fixture.set(this.channels.R, color.rgb().array()[0])
            this.fixture.set(this.channels.G, color.rgb().array()[1])
            this.fixture.set(this.channels.B, color.rgb().array()[2])
        }    
    }

    getColor(): Color{
        return this.color;
    }

    clone(): ColorPickerControll{
        return new ColorPickerControll(this.name, this.channels, this.fixture);
    }
}

export class TiltControll implements Controll{
    type: string = "tilt"

    constructor(
        public name: string = "",
        public channels: { "T": number,} = { T: 1},
        public fixture?: Fixture
    ){}

    
    set(tilt: number){
        if(this.fixture){
            this.fixture.set(this.channels.T, tilt)
        }    else{
            console.log("NO FIXTURE")
        }
    }

    clone(): TiltControll{
        return new TiltControll(this.name, this.channels, this.fixture);
    }
}


export class DimmerControll implements Controll{
    type: string = "dim"

    constructor(
        public name: string = "",
        public channels: { "d": number,} = { d: 1},
        public fixture?: Fixture
    ){}

    
    set(dim: number){
        if(this.fixture){
            this.fixture.set(this.channels.d, dim)
        }    else{
            console.log("NO FIXTURE")
        }
    }

    clone(): DimmerControll{
        return new DimmerControll(this.name, this.channels, this.fixture);
    }
}