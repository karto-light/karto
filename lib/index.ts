import { Patches, ColorPickerControll, TiltControll } from "./patches"
import { Layout } from "./layout"
import * as Color from "color"

var options = require("./../config/config").config.artnet;
export const artnet = require("artnet")(options)

export const patches = new Patches();
export const layout = new Layout(artnet);

layout.add(patches.getPatch("Generic Dimmer"), 1, null, true);
var tmh = layout.add(patches.getPatch("TMH"), 2, null, true);
var tmh = layout.add(patches.getPatch("TMH"), 50, null, true);

tmh.getControll<ColorPickerControll>("Color Picker").setColor(Color("#7743CE"))

// ( layout.get(1).getControll("Color Picker") as ColorPickerControll).setColor(new Color("#FFFFFF"));
// ( layout.get(1).getControll("Tilt") as TiltControll).set(100);

layout.get(1)


console.log(layout.get(1).values); 